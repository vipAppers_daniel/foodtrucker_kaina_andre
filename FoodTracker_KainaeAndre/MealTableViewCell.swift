//
//  MealTableViewCell.swift
//  FoodTracker_KainaeAndre
//
//  Created by Station_02 on 27/11/19.
//  Copyright © 2019 Station_02. All rights reserved.
//

import UIKit

class MealTableViewCell: UITableViewCell {
    
    //MARK: Properties
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var PhotoImageView: UIImageView!
    @IBOutlet weak var RatingControl: RatingControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
