//
//  RatingControl.swift
//  FoodTracker_KainaeAndre
//
//  Created by Station_02 on 22/11/19.
//  Copyright © 2019 Station_02. All rights reserved.
//

import UIKit

@IBDesignable class RatingControl: UIStackView {

    //MARK: Properties
    private var ratingButtons = [UIButton]()
    
    var rating = 0 {
        didSet {
            updateButtonSelectionStates()
        }
    }
    
    
    @IBInspectable var StarSize: CGSize = CGSize(width: 44.0, height: 44.0) {
        didSet {
            setupButtons()
        }
    }
    
    @IBInspectable var starCount: Int = 5{
        didSet {
            setupButtons()
        }
    }
    
    
    
    

    //MARK: initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupButtons()
        
    }
    
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        setupButtons()
        
    }
    
    
    //MARK: Button Action
    @objc func ratingButtonTapped(button: UIButton) {
        print("Button pressed 😎")
        guard let index = ratingButtons.index(of: button) else {
            fatalError( "The button, \( button ) , is not in the ratingButtons array:\( ratingButtons )")
        }
        
        // Calculate the rating of the selected button
        let selectedRating = index + 1
        
        if selectedRating == rating {
            // If the selected star represents the current rating, reset the rating to 0.
            rating = 0
        } else {
            // Otherwise set the rating to the selected star
            rating = selectedRating
        }
        print (rating)
        
    }

    
    //MARK: Private Methods
    private func setupButtons() {
        
        // clear any existing buttons
        for button in ratingButtons {
            removeArrangedSubview(button)
            button.removeFromSuperview()
        }
        ratingButtons.removeAll()
        
        let bundle = Bundle(for:type(of: self))
        let filledStar = UIImage (named: "filledStar", in:bundle, compatibleWith: self.traitCollection)
        let emptyStar = UIImage (named: "emptyStar",in: bundle, compatibleWith: self.traitCollection)
        let highlightedStar = UIImage (named: "highlightedStar", in: bundle, compatibleWith: self . traitCollection )
        
        
        for index in 0..<starCount {
            // Create the Button
            let button = UIButton()
            //set buttons image
            button.setImage(emptyStar, for: .normal)
            button.setImage(filledStar, for: .selected)
            button.setImage(highlightedStar, for: .highlighted)
            button.setImage(highlightedStar, for: [.highlighted, .selected])
            
            // Add constraints
            button.translatesAutoresizingMaskIntoConstraints = false
            button.heightAnchor.constraint(equalToConstant: 44.0).isActive = true
            button.widthAnchor.constraint(equalToConstant: 44.0).isActive = true
            
            // set the acessibility label
            button.accessibilityLabel = "Set \(index + 1) star rating"
            
            // Setup the button action
            button.addTarget(self, action: #selector(RatingControl.ratingButtonTapped(button:)), for: .touchUpInside)
            
            // Add the button to the shack
            addArrangedSubview(button)
            
            // Add the new button to the rating button array
            ratingButtons.append(button)
        }
        updateButtonSelectionStates()
    }
    private func updateButtonSelectionStates() {
        for (index, button) in ratingButtons.enumerated() {
            button.isSelected = index < rating
            
            //set the hint string for the currently selected star
            let hintString: String?
            if rating == index + 1 {
                hintString = "tap to reset the rating to 0"
            }else{
                hintString = nil
            }
            //calculate the value string
            let valueString: String
            switch (rating) {
            case 0:
                valueString = "no rating set."
            case 1:
                valueString = "1 star set."
            default:
                valueString = "\(rating) stars set."
            }
            // Assign the hint string and value string
            button.accessibilityHint = hintString
            button.accessibilityValue = valueString
    }
  }
    
}

