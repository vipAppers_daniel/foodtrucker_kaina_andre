//
//  Meal.swift
//  FoodTracker_KainaeAndre
//
//  Created by Station_02 on 26/11/19.
//  Copyright © 2019 Station_02. All rights reserved.
//

import UIKit

class Meal {
    
    //Mark: Properties
    var name: String
    var photo: UIImage?
    var rating: Int
    
    //Mark: Initialization
    init?(name: String, photo: UIImage?, rating: Int) {
        
        guard !name.isEmpty else {
            return nil
        }
    
        
        guard (rating >= 0) && (rating <= 5) else {
            return nil
        }
        
    
        // Initalize stored properties.
        self.name = name
        self.photo = photo
        self.rating = rating
        
    }
}
