//
//  FoodTracker_Kaina_eAndre_Tests.swift
//  FoodTracker_KainaeAndreTests
//
//  Created by Station_02 on 18/11/19.
//  Copyright © 2019 Station_02. All rights reserved.
//

import XCTest
@testable import FoodTracker_KainaeAndre

class FoodTracker_Kaina_eAndre_Tests: XCTestCase {

    //MARK: Meal Class Tests
    
    // Confirm that the Meal initializer returns a Meal object when passed valid parameters.
    
    func testMealInitializationSucceeds() {
        
        // Zero rating
        let zeroRatingMeal = Meal.init(name: "zero", photo: nil, rating: 0)
        XCTAssertNotNil (zeroRatingMeal)
        
        // Highest positive rating
        let positiveRatingMeal = Meal.init (name: "Positive", photo: nil, rating: 5)
        XCTAssertNotNil (positiveRatingMeal)
        
    }
    
    func testMealInitializationFails () {
        
        // Negative rating
        let negativeRatingMeal = Meal.init (name: "Negative", photo: nil, rating: -1)
        XCTAssertNil (negativeRatingMeal)
        
        // Empty String
        let emptyStringMeal = Meal.init (name: "", photo: nil, rating: 0)
        XCTAssertNil (emptyStringMeal)
        
        // Rating exceeds maximum
        let largeRatingMeal = Meal.init (name: "Large", photo: nil, rating: 6)
        XCTAssertNil (largeRatingMeal)
    }
}
